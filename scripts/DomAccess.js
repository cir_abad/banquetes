var StaticDomAccess={
    hasPath: function(path){
        var user=firebaseHelper.getDataUser();
        

        return (user && user.accessList && user.accessList[path]);
    },
    
    MAIN_TREE:{
        "_main":true,
        "empleados":{
            "name":"Recursos Humanos",
            "description":"Acceso a la sección de empleados",
            "children":{
                "agregar":{
                    "name":"Agregar Empleado",
                    "description":"Permiso para dar de alta nuevos empleados"
                },

                "editar":{
                    "name":"Editar empleado",
                    "description":"Permiso para editar un empleado"
                },
                "eliminar":{
                    "name":"Eliminar empleado",
                    "description":"Permiso para eliminar un empleado"
                },
                "habilitar":{
                    "name":"Habilitar empleado",
                    "description":"Permiso para habilitar un empleado Inactivo/Vetado"
                },
                "informacion":{
                    "name":"Ver informacion de empleado",
                    "description":"Permiso para ver informacion un empleado"
                }

            }
        },
        "usuarios":{
            "name":"Usuarios",
            "description":"Puede entrar a la lista de usuarios",
            "children":{
                "agregar":{
                    "name":"Agregar Usuario",
                    "description":"Puede dar de alta un nuevo usuario"
                    

                },
                "eliminar":{
                    "name":"Eliminar Usuario",
                    "description":"Podrá eliminar usuarios ya registrados",
                    

                },
                "editar":{
                    "name":"Editar Usuario",
                    "description":"Podrá ediar usuarios ya dados de alta",
                    

                },
                "agregar-perfil":{
                    "name":"Administrar Perfiles",
                    "description":"Puede administrar los perfiles de usuario"
                }

            }
        },
        "estadisticas":{
            "name":"Estadisticas",
            "description":"Puede entrar a la lista de usuarios",
            "children":{
                "Exportar":{
                    "name":"Exportar",
                    "description":"Puede dar de alta un nuevo usuario"
                    

                },
            },
               
        },
        "eventos":{
            "name":"Eventos",
            "description":"Tiene acceso al modulo de evento",
            "children":{
                "agregar-evento":{
                    "name":"Agregar Evento",
                    "description":"Permite crear un nuevo evento"
                },
                "gestion-datos-evento":{
                    "name":"Gestionar Datos del Evento",
                    "description":"Permite hacer cambios a la información de evento",
                    "children":{
                        "finaliza-evento":{
                            "name":"Finalizar evento",
                            "description":"Permite finalizar un evento"
                        }
                    }
                },
                "gestion-personal":{
                    "name":"Administrar personal dentro del evento",
                    "description":"Permite asignar los numeros de empleados solicitados, asignar personal y administrar confirmaciones y asistencias"
                },
                "gestion-mesas":{
                    "name":"Control de mesas",
                    "description":"Adminstra las mesas durante el evento, asigna personal  y genera los reportes de los empleados"
                }
            }

        },
        "tipos-incidencias":{
            "name":"Tipos de incidencias",
            "description":"Tiene acceso a la gestión de tipos de incidencias",
            "children":{
                "agrega-tipo":{
                    "name":"Agregar tipo",
                    "description":"Permite agregar un nuevo tipo de incidencia"
                },
                "edita-tipo":{
                    "name":"Editar tipo",
                    "description":"Modifica la informacion de un tipo"
                },
                "elimina-tipo":{
                    "name":"Eliminar tipo",
                    "description":"Permite eliminar un tipo"
                }
            }
        }
    
    
    
        
    }
};