var firebaseController={
    UserProfiles:{
        _query:null,
        getQuery: function(){
            if(!this._query){
                this._query=new QueryBinder("user-profiles",{"orderBy":"name"});
            }
            return this._query;
        },
        showProfileDialog:function(){
            var dialog=new ProfileDialog();
            PolymerUtils.Dialog.show(dialog);
        }

    },
    
    User:{
        
        openUserDialog: function(user,profile){
            var dialog=new UserDialog(profile);
            dialog.user=user;
            PolymerUtils.Dialog.showLegacy(dialog);
        },
        openUserDeleteDialog: function(user){
            var dialog=new UserDeleteDialog();
            dialog.user=user;
            PolymerUtils.Dialog.showLegacy(dialog);
        },
        deleteUser: function(user,callbacks){
            
            var updateUser = firebase.functions().httpsCallable('deleteUser');
            updateUser({uid: user.uid}).then(function(result) {
                if(callbacks && callbacks.finished){
                    callbacks.finished();
                }
                if(result.data.user){
                    if(callbacks && callbacks.success){
                        callbacks.success();
                    }   
                    PolymerUtils.Toast.show("¡Usuario eliminado con éxito!");
                }
                else{
                    if(callbacks && callbacks.fail){
                        callbacks.fail();
                    }
                    firebaseHelper.auth.showErrorToast(result.data.error);
                }
                console.log(result.data.result);
            });
        },
        updateUser: function(user,callbacks){
            
            var updateUser = firebase.functions().httpsCallable('updateUser');
            if(!user.password){
                delete user["password"];
            }

            updateUser({user: user}).then(function(result) {
                if(callbacks && callbacks.finished){
                    callbacks.finished();
                }
                if(result.data.user){
                    if(callbacks && callbacks.success){
                        callbacks.success();
                    }   
                    PolymerUtils.Toast.show("¡Usuario actualizado con éxito!");
                }
                else{
                    if(callbacks && callbacks.fail){
                        callbacks.fail();
                    }
                    firebaseHelper.auth.showErrorToast(result.data.error);
                }
                console.log(result.data.result);
            });
        },
        insertUser: function(name,email,password,photoURL,extra,finished,success,fail){
            
          console.log("Inserting user",name);
          var insertNewUser = firebase.functions().httpsCallable('insertNewUser');
          insertNewUser({email: email, name: name, passwd: password,photo:photoURL,extraData:extra}).then(function(result) {
            // Read result of the Cloud Function.
            if(finished){
              finished();
            }
            if(result.data.user){
              console.log("There's a user!");
  
              if(success){
                success(result.data.user);
              }  
              PolymerUtils.Toast.show("¡Usuario registrado con éxito!");
              
            }
            else{
              if(fail){
                fail();
              }
              console.log("No user!",result.data.error);
  
              firebaseHelper.auth.showErrorToast(result.data.error);
            }
            console.log(result.data.result);
            
          });
        }
    }
};