
        /* @polymerMixin */
        CustomClass = function(polyClass) {
          return class extends polyClass {
            ready(){
              super.ready();
              this.set("domReady",true);
			        this.dispatchEvent(new CustomEvent('dom-ready', {detail: {ready: true}}));
        
            }
            constructor(){
              super();
              var context=this;
              this.set("domReady",false);  
              this.dispatchEvent(new CustomEvent('dom-ready', {detail: {ready: false}}));
        
            }
            static get properties(){
                return{
                  _abecedario:{
                    type:Array,
                    notify:true,
                    value: ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"]
                  },
                  _localizedVaadini18n:{
                    type:Object,
                    notify:true,
                    value:   {
                      // An array with the full names of months starting
                      // with January.
                      monthNames: [
                        'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo',
                        'Junio', 'Julio', 'Agosto', 'Septiembre',
                        'Octubre', 'Noviembre', 'Diciembre'
                      ],
            
                      // An array of weekday names starting with Sunday. Used
                      // in screen reader announcements.
                      weekdays: [
                        'Domingo', 'Lunes', 'Martes', 'Miércoles',
                        'Jueves', 'Viernes', 'Sábado'
                      ],
            
                      // An array of short weekday names starting with Sunday.
                      // Displayed in the calendar.
                      weekdaysShort: [
                        'Dom', 'Lun', 'Mar', 'Mié', 'Jue', 'Vie', 'Sab'
                      ],
            
                      // An integer indicating the first day of the week
                      // (0 = Sunday, 1 = Monday, etc.).
                      firstDayOfWeek: 0,
            
                      // Used in screen reader announcements along with week
                      // numbers, if they are displayed.
                      week: 'Semana',
            
                      // Translation of the Calendar icon button title.
                      calendar: 'Calendario',
            
                      // Translation of the Clear icon button title.
                      clear: 'Limpiar',
            
                      // Translation of the Today shortcut button text.
                      today: 'Hoy',
            
                      // Translation of the Cancel button text.
                      cancel: 'Cancelar',
            
                      // A function to format given `Object` as
                      // date string. Object is in the format `{ day: ..., month: ..., year: ... }`
                      formatDate: d => {
                     //   console.log(d);
                        var monthNames=[
                          'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo',
                          'Junio', 'Julio', 'Agosto', 'Septiembre',
                          'Octubre', 'Noviembre', 'Diciembre'
                        ];
                        // returns a string representation of the given
                        return d.day+"/"+(monthNames[d.month])+"/"+d.year;
                        // object in 'MM/DD/YYYY' -format
                      },
            
                      // A function to parse the given text to an `Object` in the format `{ day: ..., month: ..., year: ... }`.
                      // Must properly parse (at least) text
                      // formatted by `formatDate`.
                      // Setting the property to null will disable
                      // keyboard input feature.
                      parseDate: text => {
                        var split=text.split("/");
                        var mes=split[1];
                       // console.log("Split",split);
                        var numero=-1;
                        var monthNames=[
                          'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo',
                          'Junio', 'Julio', 'Agosto', 'Septiembre',
                          'Octubre', 'Noviembre', 'Diciembre'
                        ];
                        for(var i=0;i<monthNames.length;i++){
                          var m=monthNames[i];
                          if(mes==m){
                            numero=i;
                            break;
                          }
                        }
                        var datos={day:Number(split[0]),month:numero,year:Number(split[2])};
                        //console.log(datos,"mesi");
                        return datos; 
                        // Parses a string in 'MM/DD/YY', 'MM/DD' or 'DD' -format to
                        // an `Object` in the format `{ day: ..., month: ..., year: ... }`.
                      },
            
                      // A function to format given `monthName` and
                      // `fullYear` integer as calendar title string.
                      formatTitle: (monthName, fullYear) => {
                        return monthName + ' ' + fullYear;
                      }
                    }
                  }
                };
            }
          
            _formatCurrency(val,sinSigno){
              if(typeof val!="number"){
                val=Number(val);
              }
              if(isNaN(val)){
                val=0;
              }
              var signo="$";
              if(sinSigno=="true"){
                signo="";
              }
              if(val)
              return signo+val.formatCurrency();
              else return signo+"0.00";
            }
            
            _formatPercentage(val){
              if((typeof val)=="string"){
                val=parseFloat(val);
              }
              return val.formatCurrency()+"%";
            }
            
            _formatPercentageEntero(val){
              if((typeof val)=="string"){
                val=parseFloat(val);
              }
              if(val)
              val=Number(val.toFixed(0));
              if(!val){
                val=0;
              }
              return (val.formatCurrency()+"%").replace(".00","");
            }
            PolymerUtils_getDateFromTimestamp(t){
              return PolymerUtils.getDateFromTimestamp(t);
            }
            PolymerUtils_getTimeString(t){
              return PolymerUtils.getTimeString(t);
            }
            PolymerUtils_getFullSpanishDate(t){
              return PolymerUtils.getFullSpanishDate(t);
            }
            PolymerUtils_getTimeStringFromString(t){
              var fecha=new Date(t);
              return PolymerUtils.getTimeString(fecha.getTime()+86400000);
            }
            PolymerUtils_getDateStringFromString(t){
              var fecha=new Date(t);
              return PolymerUtils.getDateString(fecha.getTime()+86400000);
            }
            PolymerUtils_getSpanishDateString(t){
              if(!t){
                return "-";
              }
              var fecha=PolymerUtils.getDateObjectFromString(t);
              return PolymerUtils.getDateString(fecha.getTime());
            }
            PolymerUtils_getDateString(t,yesterday){
              return PolymerUtils.getDateString(t,yesterday);
            }
            PolymerUtils_getHourString(t){
              return PolymerUtils.getHourString(t);
            }
          }
        } 