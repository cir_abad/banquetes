
        GlobalMixin = function(polyClass) {
          return class extends polyClass {
            // Code that you want common to elements.
            // If you're going to override a lifecycle method, remember that a) you
            // might need to call super but b) it might not exist
            /*connectedCallback() {
              if (super.connectedCallback) {
                super.connectedCallback();
              }
              
          */getRandomColor(){
                var random=Math.floor(Math.random()*this.materialColors.length);
                return (this.materialColors[random]);
            }
            _objectToArray(object){
              //console.log("Converting to array",object);
              if(!object){
                return [];
              }
              var arreglo=[];
              var llaves=Object.keys(object);
              
              for(var i=0;i<llaves.length;i++){
                var child=CMUtils.cloneObject(object[llaves[i]]);
                child._objectField=llaves[i];
                arreglo.push(child);
              }
              return arreglo;
            }
            static get properties(){
                return{
                  abecedario:{
                    type:Array,
                    notify:true,
                    value: ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"]
                  },
                  materialColors:{
                    type:Array,
                    notify:true,
                    value:["red",
                    "pink",
                    "purple",
                    "deep-purple",
                    "indigo",
                    "blue",
                    "light-blue",
                    "cyan",
                    "teal",
                    "green",
                    "light-green",
                    "lime",
                    "yellow",
                    "amber",
                    "orange",
                    "deep-orange",
                    "brown",
                    "grey",
                    "blue-grey"]
                  },
                  monthsNames:{
                    type:Array,
                    notify:true,
                    
    value:["enero","febrero","marzo","abril","mayo","junio","julio","agosto","septiembre","octubre","noviembre","diciembre"]
                  },
                  localizedVaadini18n:{
                    type:Object,
                    notify:true,
                    value:   {
                      // An array with the full names of months starting
                      // with January.
                      monthNames: [
                        'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo',
                        'Junio', 'Julio', 'Agosto', 'Septiembre',
                        'Octubre', 'Noviembre', 'Diciembre'
                      ],
            
                      // An array of weekday names starting with Sunday. Used
                      // in screen reader announcements.
                      weekdays: [
                        'Domingo', 'Lunes', 'Martes', 'Miércoles',
                        'Jueves', 'Viernes', 'Sábado'
                      ],
            
                      // An array of short weekday names starting with Sunday.
                      // Displayed in the calendar.
                      weekdaysShort: [
                        'Dom', 'Lun', 'Mar', 'Mié', 'Jue', 'Vie', 'Sab'
                      ],
            
                      // An integer indicating the first day of the week
                      // (0 = Sunday, 1 = Monday, etc.).
                      firstDayOfWeek: 0,
            
                      // Used in screen reader announcements along with week
                      // numbers, if they are displayed.
                      week: 'Semana',
            
                      // Translation of the Calendar icon button title.
                      calendar: 'Calendario',
            
                      // Translation of the Clear icon button title.
                      clear: 'Limpiar',
            
                      // Translation of the Today shortcut button text.
                      today: 'Hoy',
            
                      // Translation of the Cancel button text.
                      cancel: 'Cancelar',
            
                      // A function to format given `Object` as
                      // date string. Object is in the format `{ day: ..., month: ..., year: ... }`
                      formatDate: d => {
                     //   console.log(d);
                        var monthNames=[
                          'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo',
                          'Junio', 'Julio', 'Agosto', 'Septiembre',
                          'Octubre', 'Noviembre', 'Diciembre'
                        ];
                        // returns a string representation of the given
                        return d.day+"/"+(monthNames[d.month])+"/"+d.year;
                        // object in 'MM/DD/YYYY' -format
                      },
            
                      // A function to parse the given text to an `Object` in the format `{ day: ..., month: ..., year: ... }`.
                      // Must properly parse (at least) text
                      // formatted by `formatDate`.
                      // Setting the property to null will disable
                      // keyboard input feature.
                      parseDate: text => {
                        var split=text.split("/");
                        var mes=split[1];
                       // console.log("Split",split);
                        var numero=-1;
                        var monthNames=[
                          'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo',
                          'Junio', 'Julio', 'Agosto', 'Septiembre',
                          'Octubre', 'Noviembre', 'Diciembre'
                        ];
                        for(var i=0;i<monthNames.length;i++){
                          var m=monthNames[i];
                          if(mes==m){
                            numero=i;
                            break;
                          }
                        }
                        var datos={day:Number(split[0]),month:numero,year:Number(split[2])};
                        //console.log(datos,"mesi");
                        return datos; 
                        // Parses a string in 'MM/DD/YY', 'MM/DD' or 'DD' -format to
                        // an `Object` in the format `{ day: ..., month: ..., year: ... }`.
                      },
            
                      // A function to format given `monthName` and
                      // `fullYear` integer as calendar title string.
                      formatTitle: (monthName, fullYear) => {
                        return monthName + ' ' + fullYear;
                      }
                    }
                  }
                };
            }
            formatInmetalFolio(id){
              
              while(id.length<5){
                id="0"+id;
              }
              var year=(new Date().getFullYear()+"").substring(2,4);
              return "IN-"+year+id;
            }
            firstLetterCapital(tipo){
              if(!tipo){
                return null;
              }
           //   console.log("Tipoo",tipo);
              return tipo.substring(0,1).toUpperCase()+tipo.substring(1).toLowerCase();
            }
            formatCurrency(val){
              if(val)
              return "$"+val.formatCurrency();
              else return "$0.00";
            }
            formatCurrencyNoSign(val){
              if(val)
              return val.formatCurrency();
              else return "0.00";
            }
            formatPercentage(val){
              return val.formatCurrency()+"%";
            }
            CMUtils_getDateFromTimestamp(t){
              return CMUtils.getDateFromTimestamp(t);
            }
            CMUtils_getTimeString(t){
              return CMUtils.getTimeString(t);
            }
            CMUtils_getDateString(t,yesterday){
              return CMUtils.getDateString(t,yesterday);
            }
            CMUtils_getHourString(t){
              return CMUtils.getHourString(t);
            }
          }
        } 