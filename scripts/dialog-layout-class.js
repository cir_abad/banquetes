
        DialogLayoutClass = function(polyClass) {
            return class extends polyClass {
                static get properties(){
                    return{
                        _dialog:{
                            type:Object,
                            notify:true
                        }
                    };
                }
                DialogLayout_closeDialog(){
                    
                    if(this._dialog){
                        console.log("se va a cerrar el dialog");
                        this._dialog.close();
                    }
                }
                DialogLayout_notifyResize(){
                    if(this._dialog){
                        this._dialog.notifyResize();
                    }
                }

                DialogLayout_clickNegativeButton(){
                    if(this._dialog){
                        var pButton=this._dialog.querySelector(".negativeButtonClassForLayout");
                        //console.log();
                        if(pButton){
                            pButton.click();
                            //console.log("Clicked!");
                        }
                        else{
                            console.warn("There's no negative button in your custom element");
                        }
                        //this._dialog.close();
                    }
                }
                DialogLayout_clickPositiveButton(){
                    if(this._dialog){
                        var pButton=this._dialog.querySelector(".positiveButtonClassForLayout");
                        //console.log();
                        if(pButton){
                            pButton.click();
                            //console.log("Clicked!");
                        }
                        else{
                            console.warn("There's no positive button in your custom element");
                        }
                        //this._dialog.close();
                    }
                }
            }
          } 